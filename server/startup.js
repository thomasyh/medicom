import { Meteor } from 'meteor/meteor';
import { Parties } from '../imports/api/parties';
import { Patients } from '../imports/api/patients';

Meteor.startup(() => {
  if (Parties.find().count() === 0) {
    const parties = [{
      'name': 'Dubstep-Free Zone',
      'description': 'Fast just got faster with Nexus S.'
    }, {
      'name': 'All dubstep all the time',
      'description': 'Get it on!'
    }, {
      'name': 'Savage lounging',
      'description': 'Leisure suit required. And only fiercest manners.'
    }];

    parties.forEach((party) => {
      Parties.insert(party)
    });
  }
  if (Patients.find().count() === 0) {

    const patients = [{
        'name': 'Patient One'

    }, {
        'name': 'Patient Two'
    }, {
        'name': 'Patient Three'
    }];

    patients.forEach((patient) => {
      Patients.insert(patient)
  });

  }
});
