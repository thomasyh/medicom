import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';

import './home.html';
import { Patients } from '../../../api/patients';

class Home {
    constructor($scope, $reactive) {
        'ngInject';
        $reactive(this).attach($scope);
        //$scope.patients = [{
        //    'name': 'Patient One'
        //
        //}, {
        //    'name': 'Patient Two'
        //}, {
        //    'name': 'Patient Three'
        //}];


        this.helpers({
            patients() {
                //console.log("Patients:");
                //console.log(Patients.find({});
                return Patients.find({});
            }
        });

        //$scope.tabs.tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    }
}

const name = 'home';

// create a module
export default angular.module(name, [
    angularMeteor,
    uiRouter,
    //PartyAdd,
    //PartyRemove
]).component(name, {
    templateUrl: `imports/ui/components/${name}/${name}.html`,
    controllerAs: name,
    controller: Home
})
.config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('home', {
            url: '/home',
            template: '<home></home>'
        });
}
